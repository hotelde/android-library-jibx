HOTEL DE Android App
====================

Huzzah!


JiBX binding for parsing XML from the HOTEL DE Webservice
-----------------------------------------

This project is for building a [JiBX](http://jibx.sourceforge.net/) binding for web services run by [HOTEL DE](https://www.hotel.de)


This acts as a Data Layer for our [Java/Android API project](https://bitbucket.org/hotelde/android-library).
