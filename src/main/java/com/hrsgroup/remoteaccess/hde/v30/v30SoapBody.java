package com.hrsgroup.remoteaccess.hde.v30;

import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelResResponseType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRS;

import de.hotel.remoteaccess.v28.model.GetPropertyDescriptionResponse;
import de.hotel.remoteaccess.v28.model.PropertyDescriptionExtendedResponse;

public class v30SoapBody {
    private OTAHotelAvailRS otaHotelAvailRS;
    private HotelResResponseType hotelResResponseType;

    public OTAHotelAvailRS getOtaHotelAvailRS() {
        return otaHotelAvailRS;
    }

    public void setOtaHotelAvailRS(OTAHotelAvailRS otaHotelAvailRS) {
        this.otaHotelAvailRS = otaHotelAvailRS;
    }

    public HotelResResponseType getOtaHotelResRS() {
        return hotelResResponseType;
    }

    public void setOtaHotelResRS(HotelResResponseType hotelResResponseType) {
        this.hotelResResponseType = hotelResResponseType;
    }

    public HotelResResponseType getHotelResResponseType() {
        return hotelResResponseType;
    }

    public void setHotelResResponseType(HotelResResponseType hotelResResponseType) {
        this.hotelResResponseType = hotelResResponseType;
    }

}
