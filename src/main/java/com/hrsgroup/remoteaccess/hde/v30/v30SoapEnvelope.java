package com.hrsgroup.remoteaccess.hde.v30;

public class v30SoapEnvelope {
    private v30SoapBody body;

    public v30SoapBody getBody() {
        return body;
    }

    public void setBody(v30SoapBody body) {
        this.body = body;
    }
}
