package de.hotel.remoteaccess.v28;

import de.hotel.remoteaccess.v28.model.*;

public class v28SoapBody {
    private GetPropertyDescriptionResponse propertyDescriptionResponse;
    private GetPropertyReviewsResponse propertyReviewsResponse;
    private CustomerCheckExistenceResponse customerCheckExistenceResponse;
    private GetCustomerDataResponse customerDataResponse;
    private CheckReservationStatusResponse reservationStatusResponse;
    private CancelReservationResponse cancelReservationResponse;
    private GetLocationsResponse getLocationsResponse;
    private DetermineLocationNumberResponse determineLocationNumberResponse;

    public GetPropertyDescriptionResponse getPropertyDescriptionResponse() {
        return propertyDescriptionResponse;
    }

    public void setPropertyDescriptionResponse(GetPropertyDescriptionResponse propertyDescriptionResponse) {
        this.propertyDescriptionResponse = propertyDescriptionResponse;
    }

    public GetPropertyReviewsResponse getPropertyReviewsResponse() {
        return propertyReviewsResponse;
    }

    public void setPropertyReviewsResponse(GetPropertyReviewsResponse propertyReviewsResponse) {
        this.propertyReviewsResponse = propertyReviewsResponse;
    }

    public CustomerCheckExistenceResponse getCustomerCheckExistenceResponse() {
        return customerCheckExistenceResponse;
    }

    public void setCustomerCheckExistenceResponse(CustomerCheckExistenceResponse customerCheckExistenceResponse) {
        this.customerCheckExistenceResponse = customerCheckExistenceResponse;
    }

    public GetCustomerDataResponse getCustomerDataResponse() {
        return customerDataResponse;
    }

    public void setCustomerDataResponse(GetCustomerDataResponse customerDataResponse) {
        this.customerDataResponse = customerDataResponse;
    }

    public CheckReservationStatusResponse getReservationStatusResponse() {
        return reservationStatusResponse;
    }

    public void setReservationStatusResponse(CheckReservationStatusResponse reservationStatusResponse) {
        this.reservationStatusResponse = reservationStatusResponse;
    }

    public CancelReservationResponse getCancelReservationResponse() {
        return cancelReservationResponse;
    }

    public void setCancelReservationResponse(CancelReservationResponse cancelReservationResponse) {
        this.cancelReservationResponse = cancelReservationResponse;
    }

    public GetLocationsResponse getGetLocationsResponse() {
        return getLocationsResponse;
    }

    public void setGetLocationsResponse(GetLocationsResponse getLocationsResponse) {
        this.getLocationsResponse = getLocationsResponse;
    }

    public DetermineLocationNumberResponse getDetermineLocationNumberResponse() {
        return determineLocationNumberResponse;
    }

    public void setDetermineLocationNumberResponse(DetermineLocationNumberResponse determineLocationNumberResponse) {
        this.determineLocationNumberResponse = determineLocationNumberResponse;
    }
}
