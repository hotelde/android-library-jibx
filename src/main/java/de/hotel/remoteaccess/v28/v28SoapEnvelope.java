package de.hotel.remoteaccess.v28;

public class v28SoapEnvelope {
    private v28SoapBody body;

    public v28SoapBody getBody() {
        return body;
    }

    public void setBody(v28SoapBody body) {
        this.body = body;
    }
}
